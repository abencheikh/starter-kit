For Linux users:

- cd starter-kit/app/ui && yarn install

- cd ../core && yarn install

- cd ../../ && yarn docker:start

For Mac / Windows users:

- yarn core:shell

- yarn install

- exit

- yarn ui:shell

- yarn install

- exit

- yarn docker:start