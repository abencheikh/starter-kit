import React from 'react'
import Router from 'next/router'
import axios from 'axios'
import Title from '../components/Title.js'

export default class extends React.Component {
  static async getInitialProps () {
    const response = await axios.get('http://localhost:3000/api/date')

    return ({ date: response.data.date })
  }

  render () {
    const { date } = this.props
    return (
      <div>
        <Title content='Hello World !' />
        <div>{date}</div>
      </div>
    )
  }
}
