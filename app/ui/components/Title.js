import React from 'react'

export default ({content}) => (
  <div>
    <h1>{content}</h1>
    <style jsx>{`
            h1 {
                font-size: 24px;
                color: red;
            }
        `}</style>
  </div>
)
