import express from 'express'

const app = express()

app.get('/', (req, res) => {
  res.send('hello')
})

app.get('/api/date', (req, res) => {
  res.json({
    date: new Date()
  })
})

app.listen(8080, () => {
  console.log('App listening on port 8080')
})
